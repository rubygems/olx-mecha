require 'olx/mecha/version'

require 'watir'
require 'headless'

module Olx
  module Mecha
    LOGIN_URL = 'https://www3.olx.com.br/account/form_login'.freeze
    LOGON_URL = 'https://www3.olx.com.br/account/userads/'.freeze

    if ENV['HEADLESS']
      @headless = Headless.new
      @headless.start
    end

    def self.login(username, password)
      @browser = Watir::Browser.new
      @browser.goto LOGIN_URL
      @browser.text_field(id: 'login_email').set username
      @browser.text_field(id: 'login_password').set password
      @browser.button(type: 'submit').click

      LOGON_URL == @browser.url
    end

    def self.destroy
      @browser.quit if defined? @browser
      @headless.destroy if defined? @headless
    end
  end
end
